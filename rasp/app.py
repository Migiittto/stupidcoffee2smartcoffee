# -*- coding: utf-8 -*-
# Run this with gunicorn: gunicorn app:app -b 1.2.3.4:5665

from flask import Flask, request, make_response, jsonify
app = Flask(__name__)
import subprocess, sys

path ="/home/pi/Desktop/stupidcoffee2smartcoffee/rasp/"

@app.route('/coffee/', methods=["GET", "POST"])
def toggle_cofee():
    text=""
    stdout = subprocess.check_output("python2 "+path+"kahvia.py", stderr=subprocess.PIPE, shell=True).decode(sys.stdout.encoding)
    for s in stdout.splitlines():
        text += s +"<br />"
    return text

if __name__ == "__main__":
    app.run(host='0.0.0.0')