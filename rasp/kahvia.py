#!/usr/bin/env python
########################################################################
# Filename    : kahvia.py
# Description : Toggle for relay
########################################################################
import RPi.GPIO as GPIO
import time

relayPin = 11    # define the relayPin
relayState = False

def setup():
	print('Varataan vaylat...')
	GPIO.setmode(GPIO.BOARD)       # Numbers GPIOs by physical location
	GPIO.setup(relayPin, GPIO.OUT)   # Set relayPin's mode is output
	
def destroy():
	GPIO.output(relayPin, GPIO.LOW)     # relay off
	GPIO.cleanup()                     # Release resource

if __name__ == '__main__':     # Program start from here
	setup()
	GPIO.output(relayPin,True)
	print("Toglattiin keittimen virta!")
	time.sleep(1)
	GPIO.output(relayPin,False)
	destroy()

